# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Bernhard Landauer <oberon@manjaro.org>
# Maintainer: Frede Hundewadt <fhatmanjarorg>

_ver=0.15.15dev

pkgname=('manjaro-tools-base-git'
         'manjaro-tools-pkg-git'
         'manjaro-tools-iso-git'
         'manjaro-tools-yaml-git')
pkgbase=manjaro-tools-git
pkgver=r3045.7e49d96
pkgrel=1
pkgdesc="Development tools for Manjaro Linux"
arch=('any')
url="https://gitlab.manjaro.org/tools/development-tools/manjaro-tools"
license=('GPL-3.0-or-later')
groups=('manjaro-tools')
makedepends=('git' 'docbook2x')
_commit=7e49d96e72d54389aff6bd1cc5d1242711f0c898
source=("git+$url.git#commit=${_commit}")
sha256sums=('c6b25ce3a9261c63d3a3b1fa4206de641c5417077d876e24df91ff1767deec52')

pkgver() {
    cd "${srcdir}/${pkgbase%-git}"
    printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short=7 HEAD)"
}

prepare() {
    cd "${srcdir}/${pkgbase%-git}"
    sed -e "s/^Version=.*/Version=${_ver}/" -i Makefile

    # patches here
    
}

build() {
    cd "${srcdir}/${pkgbase%-git}"
    make PREFIX=/usr
}

package_manjaro-tools-base-git() {
    pkgdesc+=" (base tools)"
    depends=('gnupg' 'openssh' 'os-prober' 'pacman-mirrors' 'rsync')
    optdepends=('manjaro-tools-pkg-git: Manjaro Linux package tools'
                'manjaro-tools-iso-git: Manjaro Linux iso tools'
                'manjaro-tools-yaml-git: Manjaro Linux yaml tools'
                'haveged: for faster keygen tasks')
    provides=("${pkgname%-git}=${_ver}")
    conflicts=("${pkgname%-git}")
    backup=("etc/${pkgbase%-git}/${pkgbase%-git}.conf")

    cd "${srcdir}/${pkgbase%-git}"
    make PREFIX=/usr DESTDIR="${pkgdir}" install_base
}

package_manjaro-tools-pkg-git() {
    pkgdesc+=" (packaging tools)"
    depends=('manjaro-tools-base-git' 'namcap')
    provides=("${pkgname%-git}=${_ver}")
    conflicts=("${pkgname%-git}" 'devtools')

    cd "${srcdir}/${pkgbase%-git}"
    make PREFIX=/usr DESTDIR="${pkgdir}" install_pkg
}

package_manjaro-tools-yaml-git() {
    pkgdesc+=" (yaml tools)"
    depends=('calamares-tools' 'manjaro-iso-profiles-base' 'manjaro-tools-base-git'
             'ruby-kwalify')
    provides=("${pkgname%-git}=${_ver}")
    conflicts=("${pkgname%-git}")

    cd "${srcdir}/${pkgbase%-git}"
    make PREFIX=/usr DESTDIR="${pkgdir}" install_yaml
}

package_manjaro-tools-iso-git() {
    pkgdesc='Development tools for Manjaro Linux (ISO tools)'
    depends=('dosfstools' 'git' 'grub' 'libisoburn' 'manjaro-tools-yaml-git'
             'mkinitcpio' 'mktorrent' 'snapd' 'squashfs-tools')
    provides=("${pkgname%-git}=${_ver}")
    conflicts=("${pkgname%-git}")

    cd "${srcdir}/${pkgbase%-git}"
    make PREFIX=/usr DESTDIR="${pkgdir}" install_iso
}
